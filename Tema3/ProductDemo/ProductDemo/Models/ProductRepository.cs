﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductDemo.Models
{
    public class ProductRepository
    {
        readonly List<Product> _porducts = new List<Product>()
        {
            new Product
            {
                Id=1,
                Name="Coffe",
                Description="Black coffe"
            },
            new Product
            {
                Id=2,
                Name="Milk",
                Description="Bio Milk"
            },
            new Product
            {
                Id=3,
                Name="Sugar",
                Description="Brown sugar"
            },
            new Product
            {
                Id=4,
                Name="Flour",
                Description="Bread flour"
            }

            };
        public List<Product> GetAllProducts => _porducts;
        public Product GetProductById(int id)
        {
            return _porducts.SingleOrDefault(x => x.Id == id);
        }
    }
}
