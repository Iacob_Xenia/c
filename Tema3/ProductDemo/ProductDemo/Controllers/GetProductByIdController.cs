﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductDemo.Models;

namespace ProductDemo.Controllers
{
    public class GetProductByIdController : Controller
    {
        readonly ProductRepository _repo;
        public GetProductByIdController(ProductRepository repo)
        {
            _repo = repo;
        }
        public IActionResult Index(int id)
        {
            var product = _repo.GetProductById(id);
            return View(product);
        }
    }
}