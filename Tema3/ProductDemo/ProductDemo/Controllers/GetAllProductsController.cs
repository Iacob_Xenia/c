﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductDemo.Models;

namespace ProductDemo.Controllers
{
    public class GetAllProductsController : Controller
    {
        readonly ProductRepository _repo;
        public GetAllProductsController(ProductRepository repo)
        {
            _repo = repo;
        }
        public IActionResult Index()
        {
            var products = _repo.GetAllProducts;
            return View(products);
        }
    }
}