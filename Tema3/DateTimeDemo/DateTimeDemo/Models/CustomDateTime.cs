﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateTimeDemo.Models
{
    public class CustomDateTime
    {
        public string day { get; set; }
        public DateTime date { get; set; }
        public TimeSpan time { get; set; }
    }
}
