﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateTimeDemo.Models
{
    public class DateTimeRepository
    {

        readonly CustomDateTime _customDateTimes = new CustomDateTime
        {
                day=DateTime.Now.DayOfWeek.ToString(),
                date= DateTime.Now,
                time=DateTime.Now.TimeOfDay


        };

        public CustomDateTime GetDateTime => _customDateTimes;
    }
}
