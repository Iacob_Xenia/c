﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateTimeDemo.Models;
using Microsoft.AspNetCore.Mvc;

namespace DateTimeDemo.Controllers
{
    public class CustomDateTimeController : Controller
    {
        readonly DateTimeRepository _repo;

        public CustomDateTimeController(DateTimeRepository repo)
        {
            _repo = repo;
        }
        public IActionResult Index()
        {
            var dateTime = _repo.GetDateTime;
            return View(dateTime);
        }
    }
}