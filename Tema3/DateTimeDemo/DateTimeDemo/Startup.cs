﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DateTimeDemo.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace DateTimeDemo
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddTransient<DateTimeRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseMvc(ConfigureRoutes);
           // app.UseMvcWithDefaultRoute();
        }

        private void ConfigureRoutes(IRouteBuilder routes)
        {
            routes.MapRoute(null, "{controller=CustomDateTime}/{action=index}/{id?}");
        }
    }
}
