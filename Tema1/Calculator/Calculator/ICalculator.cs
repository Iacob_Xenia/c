﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
   public interface ICalculator
    {
        double Addition(double x, double y);
        double Subtraction(double x, double y);
        double Divide(double x, double y);
        double Multiplication(double x, double y);

    }
}
