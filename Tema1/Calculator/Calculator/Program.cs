﻿using System;

namespace Calculator
{
    class Program: Calculator
    {
        static void Main(string[] args)
        {
            ICalculator calculator = new Calculator();
            Console.WriteLine("Meniu Calculator");
            double result = 0;
           

            Console.WriteLine("Enter first number");
            double num1 = double.Parse(Console.ReadLine());

          
            Console.WriteLine("Enter second number");
            double num2 = double.Parse(Console.ReadLine());

    
            Console.WriteLine("Enter operator");
            string op = Console.ReadLine();

            switch (op)
            {

                case "+":
                    result = calculator.Addition(num1, num2);
                    break;
                case "-":
                    result = calculator.Subtraction(num1, num2);
                    break;
                case "*":
                    result = calculator.Multiplication(num1, num2);
                    break;
                case "/":
                    result = calculator.Divide(num1, num2);
                    break;

            }
            Console.WriteLine("Result = " + result);
        }
    }
}
