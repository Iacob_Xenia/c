﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public class Calculator:ICalculator
    {
        public double Addition(double x, double y) => x + y;
        //{
        //    return x + y;
        //}

        public double Subtraction(double x, double y) => x - y;
        public double Divide(double x,double y)
        {
            if (y == 0) throw new Exception("Divide by 0");
            return x / y;
        }

        public double Multiplication(double x, double y) => x * y;


    }
}
