﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDemo
{public enum GradProfesor
        {
            definitivat=0,
            grad_1=1,
            grad_2=2,
            doctorat=3
        };
   public class Profesor:Person
    {
        GradProfesor privateObj;

        public Profesor()
        {

        }
        public GradProfesor GradProfesor
        {
            get
            {
                return privateObj;
            }
            set
            {
                privateObj = value;
            }
        }

        public override string ToString()
        {
            return "Nume:" + Nume + '\n'
                + "Prenume:" + Prenume + '\n'
                + "Adresa:" + Adresa + '\n' + 
                "Grad:" + GradProfesor + '\n';
        }
    }
}
