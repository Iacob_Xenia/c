﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDemo
{
    public class Person
    {
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string Adresa { get; set; }

        public override string ToString()
        {
            return "Nume:" + Nume + '\n' + "Prenume:" + Prenume + '\n' + "Adresa:" + Adresa + '\n';
        }
    }
}
