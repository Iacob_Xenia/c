﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDemo
{
    public class Student:Person
    {
        public string NrMatricol { get; set; }
        public double Medie { get; set; }
        public override string ToString()
        {
            return "Nume:" + Nume + '\n' + "Prenume:" + Prenume + '\n' + "Adresa:" + Adresa + '\n' + "NrMatricol:" + NrMatricol + '\n' + "Medie" + Medie + '\n';
        }
    }
}
