﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> personList = new List<Person>();

            Person p1 = new Person
            {
                Nume = "Popescu",
                Prenume = "Alexandru",
                Adresa = "Calea Mare, nr 5"
            };

            Person s1 = new Student
            {
                Nume = "Lupu",
                Prenume = "Ilinca",
                Adresa = "Victoriei 5",
                NrMatricol="12345",
                Medie=9.34
            };
            Person pr1 = new Profesor
            {
                Nume = "Muresean",
                Prenume = "Vasile",
                Adresa = "Independentei, nr 21",
                GradProfesor=GradProfesor.doctorat
            };

            personList.Add(p1);
            personList.Add(s1);
            personList.Add(pr1);
            Console.WriteLine("Lista initiala");
            foreach (Person p in personList)
            {
                Console.WriteLine(p.GetType());
                Console.WriteLine(p);

            }
            personList.Sort((x, y) => x.Nume.CompareTo(y.Nume));
            Console.WriteLine("Lista ordonata dupa nume");
            foreach (Person p in personList)
            {
                Console.WriteLine(p.Nume);
            }
            personList.Sort((x, y) => x.Prenume.CompareTo(y.Prenume));
            Console.WriteLine("Lista ordonata dupa preume");
            foreach (Person p in personList)
            {
                Console.WriteLine(p.Prenume);
            }
            Console.ReadLine();
        }
    }
}
