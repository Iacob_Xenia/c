﻿using System;
using System.Collections.Generic;
using System.Linq;
using AnimalDemo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
namespace AnimalTest
{
    [TestClass]
    public class AnimalRepositoryTest
    {
        [TestMethod]
        public void GetData_Test()
        {
            Mock<IAnimalRepository> repoMock = new Mock<IAnimalRepository>();

            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Clarck",
                Description = "Is a Parrot"
            };
            repoMock.Setup(x => x.GetById(2)).Returns(a1);

            AnimalController controller = new AnimalController(repoMock.Object);

            Animal result = controller.GetById(2);

            Assert.AreEqual(a1.Id, result.Id);
            Assert.AreEqual(a1.Name, result.Name);
            Assert.AreEqual(a1.Description, result.Description);
        }

        [TestMethod]
        public void GetAllData_Test()
        {
            Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();

            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Clarck",
                Description = "Is a Parrot"
            };
            Animal a2 = new Tiger
            {
                Id = 2,
                Name = "Leo",
                Description = "Is a Tiger"
            };

            List<Animal> animals = new List<Animal>();
            animals.Add(a1);
            animals.Add(a2);

            mock.Setup(x => x.GetAll()).Returns(animals);

            AnimalController controler = new AnimalController(mock.Object);

            List<Animal> result = controler.GetAll().ToList();

            Assert.AreEqual(2, result.Count);
        }
    }
}
    

