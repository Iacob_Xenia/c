﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Clarck",
                Description = "Is a Parrot"
            };

            Animal a2 = new Tiger
            {
                Id = 2,
                Name = "Leo",
                Description = "Is a Tiger"
            };

            AnimalRepository repo = new AnimalRepository();
            repo.Add(a1);
            repo.Add(a2);

            Animal first = repo.GetById(1);
            Animal sec = repo.GetById(2);
            Console.WriteLine("Animals GetById");
            Console.WriteLine(first);
            Console.WriteLine(sec);
            Console.WriteLine("All Animals");
            List<Animal> animals=repo.GetAll();
            foreach (Animal a in animals)
            {
                Console.WriteLine(a);
            }
            repo.Delete(1);
            
            foreach (Animal a in animals)
            {
                Console.WriteLine(a);
            }
            Console.ReadLine();

           repo.Update(2);

        }
    }
}
