﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AnimalDemo
{
    public class AnimalRepository : IAnimalRepository
    {
        readonly List<Animal> _animals = new List<Animal>();
        public void Add(Animal animal)
        {
            _animals.Add(animal);
            Console.WriteLine("Animal added");
        }
        public Animal GetById(int id)
        {
            return _animals.SingleOrDefault(x => x.Id == id);
        }
        public void Delete(int id)
        {
            var currentAnimal = _animals.Find(x => x.Id == id);
            _animals.Remove(currentAnimal);
            Console.WriteLine("Animal deleted");
        }


        public void Update(int id)
        {
            
            
            var currentAnimal = _animals.Find(x => x.Id == id);
            Console.WriteLine("New Name:");
            var name = Console.ReadLine();
            currentAnimal.Name = name;
            Console.WriteLine("New Description:");
            var description= Console.ReadLine();
            currentAnimal.Description = description;
            Console.WriteLine("Succes Update");

        }

        public List<Animal> GetAll() => _animals;
    }
}
