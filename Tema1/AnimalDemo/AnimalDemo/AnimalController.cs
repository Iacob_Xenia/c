﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalDemo
{
   public class AnimalController
    {

        readonly IAnimalRepository _animalRepository;
        public AnimalController(IAnimalRepository animalRepository)
        {
            _animalRepository = animalRepository;
            // personRepository = new PersonRepository();
        }
        public AnimalController()
        {
            _animalRepository = new AnimalRepository();
        }

        public void Insert(Animal a)
        {
            _animalRepository.Add(a);
        }

        public Animal GetById(int id)
        {
            return _animalRepository.GetById(id);
        }

        public void Update(int id)
        {
            _animalRepository.Update(id);
        }

        public void Delete(int id)
        {
            _animalRepository.Delete(id);
        }

        public List<Animal> GetAll() => _animalRepository.GetAll();
    }
}
