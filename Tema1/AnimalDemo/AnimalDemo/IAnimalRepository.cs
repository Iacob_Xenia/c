﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalDemo
{
    public interface IAnimalRepository
    {
        void Add(Animal animal);

        Animal GetById(int id);

        void Delete(int id);

        void Update(int id);

        List<Animal> GetAll();
    }
}
