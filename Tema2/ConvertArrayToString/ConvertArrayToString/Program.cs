﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertArrayToString
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.Write("Input number of elements : ");
            int n = Convert.ToInt32(Console.ReadLine());
            string[] array = new string[n];
            for (int i = 0; i < n; i++)
            {
                Console.Write("Element[{0}] : ", i);
                array[i] = Console.ReadLine();
            }

            string newstring = String.Join(", ", array
                          .Select(s => s.ToString())
                          .ToArray());
            Console.Write("The string created with the array :\n");
            Console.WriteLine(newstring);
            Console.ReadLine();
        }
    }
}
