﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FindAString
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] cities =
            {
                "ROME","LONDON","NAIROBI","CALIFORNIA","ZURICH","NEW DELHI","AMSTERDAM","ABU DHABI", "PARIS"
            };

            Console.Write("Cities : 'ROME','LONDON','NAIROBI','CALIFORNIA','ZURICH','NEW DELHI','AMSTERDAM','ABU DHABI','PARIS'");

            Console.Write("\nStarting character : ");
            string startCharacter = Convert.ToString(Console.ReadLine());
            Console.Write("Ending character : ");
            string endCharacter = Convert.ToString(Console.ReadLine());

            var _result = from x in cities
                          where x.StartsWith(startCharacter)
                          where x.EndsWith(endCharacter)
                          select x;
            foreach (var city in _result)
            {
                Console.Write("The city starting with {0} and ending with {1} is : {2}\n", startCharacter, endCharacter, city);
            }

            Console.ReadLine();
        }
    }
}
